USPGo - Contributors
USPgo is a free open source project and everyone is more than welcome
to contribute with either ideas, bug reports or source code.
If you believe you can help, please read this document thoroughly.

How to start
If you want to contribute an idea or suggestion, or you have a specific
inquiry, please refer to the following link:
https://gitlab.com/uspgo