from flask import Flask, request
from math import sin, cos, sqrt, atan2, radians
from flask_restful import Resource, Api, reqparse

app = Flask(__name__)
api = Api(app)
locations = {}
def distance(lat1, lon1, lat2, lon2):
    R = 6373.0

    lat1 = radians(lat1)
    lon1 = radians(lon1)
    lat2 = radians(lat2)
    lon2 = radians(lon2)

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c
    return distance

class set_location(Resource):
    
    def post(self):
        global locations
        parser = reqparse.RequestParser()
        parser.add_argument('username', type=str)
        parser.add_argument('latitude', type=float)
        parser.add_argument('longitude', type=float)
        args = parser.parse_args()
        locations[args['username']] = {'latitude' : args['latitude'], 'longitude' : args['longitude']}
        print(locations)
        return {"status" : 200, "msg" : "current user location was set"}

class get_location(Resource):
    def post(self):
        global locations
        parser = reqparse.RequestParser()
        parser.add_argument('username', type=str)
        parser.add_argument('latitude', type=float)
        parser.add_argument('longitude', type=float)
        parser.add_argument('radius', type=float)
        args = parser.parse_args()
        print("locations")
        print(locations)
        users = []
        for user in locations:
            d = distance(args['latitude'], args['longitude'], locations[user]['latitude'], locations[user]['longitude'])
            if d <= args['radius'] and user != args['username']:
                users.append({ 'username' : user, 'latitude' : locations[user]['latitude'], 'longitude' : locations[user]['longitude']})
        return users
api.add_resource(set_location, '/location/set')
api.add_resource(get_location, '/location/get')
if __name__ == '__main__':
    app.run(debug = True)
